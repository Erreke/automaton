import firebase from 'firebase/app';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyD8ymZdPi7xEgFWvBkX9B4402pKnerAWbY',
  authDomain: 'automat-3e8a4.firebaseapp.com',
  databaseURL: 'https://automat-3e8a4.firebaseio.com',
  projectId: 'automat-3e8a4',
  storageBucket: 'automat-3e8a4.appspot.com',
  messagingSenderId: '1050573956434',
  appId: '1:1050573956434:web:4cfe6ba7e3d72cac90868b',
};

export const firestoreApp = firebase.initializeApp(config);

export default firestoreApp.firestore();
