export default {
  clients: {},
  discounts: {},
  clientsQueue: [],
  discountDeadlineSaving: false,
  isClientSaving: false,
  clientCreatingErrors: [],
  isClientsLoading: false,
  isTariffAdding: false,
  isTariffAddingError: false,
};
