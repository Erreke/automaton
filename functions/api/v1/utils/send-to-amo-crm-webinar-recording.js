const AmoCrm = require('./amocrm');

module.exports = function sendToAmoCRM(name, email, phone) {
  const config = {
    project: 'automaton',
    userLogin: 'adamcigelnik@mail.ru',
    userHash: '7ba083b44d3008e2df2551f74b75a020f09ea13c',
    data: {
      name,
      email,
      phone
    },
    city: 'other',
    note: 'Был на вебинаре',
    contactTags: ['Из формы webinar.automaton.bz'],
    newLeadName: name,
    leadTags: ['Из формы webinar.automaton.bz'],
    pipelineIds: {
      other: 2220133,   // Вебинар
    },
    statusIds: {
      other: 31039081,  // ЗАПИСАЛИСЬ на вебинар
    },
  };

  const amo = new AmoCrm(config);

  return amo
    .send()
    .then(response => response);
};
