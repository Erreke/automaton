const axios = require('axios');

function getLastWebinarId() {
  return axios.get('https://online.bizon365.ru/api/v1/webinars/reports/getlist', {
    headers: {
      'X-token': 'S-wXdE9SHB-xvXuVcHrH-WvQ_NcBSrZGwXOE5SBBbQw7_EcBH',
    },
    params: {
      skip: 0,
      limit: 5,
    }
  })
  .then((response) => {
    return response.data.list[0].webinarId;
  })
  .catch((error) => {
    console.error(error);

    throw new Error('Unable getting webinars list');
  });
}

function getReports(id) {
  return axios.get('https://online.bizon365.ru/api/v1/webinars/reports/get', {
    headers: {
      'X-token': 'S-wXdE9SHB-xvXuVcHrH-WvQ_NcBSrZGwXOE5SBBbQw7_EcBH',
    },
    params: {
      webinarId: id,
    }
  })
  .then((response) => {
    return response.data;
  })
  .catch((error) => {
    console.error(error);

    throw new Error(`Unable getting report for webinar ${id}`);
  });
}

module.exports = async function getBizonReport() {
  const webinarId = await getLastWebinarId();
  const reports = await getReports(webinarId);

  return {
    users: JSON.parse(reports.report.report),
    messages: JSON.parse(reports.report.messages),
    messagesTS: JSON.parse(reports.report.messagesTS),
  };
}
