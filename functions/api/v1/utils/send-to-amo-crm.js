const AmoCrm = require('./amocrm');

module.exports = function sendToAmoCRM(name, phone) {
  const config = {
    project: 'automaton',
    userLogin: 'adamcigelnik@mail.ru',
    userHash: '7ba083b44d3008e2df2551f74b75a020f09ea13c',
    data: {
      name,
      email: '',
      phone
    },
    city: 'other',
    note: 'Хочу на вебинар',
    contactTags: ['Из главной automaton.bz'],
    newLeadName: name,
    leadTags: ['Из главной automaton.bz'],
    pipelineIds: {
      other: 2220133,   // Вебинар
    },
    statusIds: {
      other: 31190359,  // ЗАПИСАЛИСЬ на вебинар
    },
  };

  const amo = new AmoCrm(config);

  return amo
    .send()
    .then(response => response);
};
