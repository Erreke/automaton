const axios = require('axios');
const { INSTIME_HOST } = require('../../../config');

module.exports = function getInstimeInfo(id) {
  if (!id) return Promise.resolve(false);

  return axios.get(`${INSTIME_HOST}/api/user/view`, {
    params: {
      token: '8JJYunxF6YJLzQDydFUhwn9HjnZuRM4CrFNktynBBTf7LkJrxJMKcALZJeMzMNRN',
      user: id
    }
  })
    .then((response) => {
      const { data } = response.data;

      if (response.data.status) {
        return {
          instagram: data.username,
          phone: data.phone,
          email: data.email,
          balance: data.balance,
          totalEarned: data.total_earned,
          instimePurchasedAt: new Date(data.subscription.created_at * 1000),
          instimeExpiredAt: new Date(data.subscription.ended_at * 1000),
        };
      }

      return null;
    })
    .catch((error) => {
      console.error(error);

      throw new Error('Unable getting info from instime');
    });
}
