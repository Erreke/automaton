const { admin } = require('../../../admin');

module.exports = function isUserAdmin(uid) {
  return admin.auth()
    .getUser(uid)
    .then(user => {
      return user.email === 'ereke@list.ru'
       || user.email === 'trader-kz@mail.ru'
       || user.email === 'amuxamedali@bk.ru';
    });
}