const axios = require('axios');
const querystring = require('querystring');
const { INSTIME_HOST } = require('../../../config');

module.exports = function getInstimePaymentUrl(referralCode) {
  if (!referralCode) return Promise.resolve(false);

  return axios.post(
    `${INSTIME_HOST}/api/payment/buy-subscription-link`,
    querystring.stringify({
      paymentSystem: 4,
      packageType: 'twelveMonthPremium',
    }),
    {
      params: {
        token: '8JJYunxF6YJLzQDydFUhwn9HjnZuRM4CrFNktynBBTf7LkJrxJMKcALZJeMzMNRN',
        user: referralCode
      }
    }
  )
    .then((response) => {
      const { status, link } = response.data;

      if (status) {
        return link;
      }

      return false;
    })
    .catch((error) => {
      console.error(error);

      throw new Error('Unable getting payment url from Instime');
    });
}
