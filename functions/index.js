const functions = require('firebase-functions');

const api = require('./api');
const approveOrRejectFinances = require('./finances-approve-or-reject');
const notifyAdminAboutNewUser = require('./notify-about-new-user');
const checkClientInAmo = require('./check-client-in-amo');
// const updateFollowerInfoInAllParents = require('./update-follower-info-in-all-parents');
// const handleBusinessPlaceActivation = require('./handle-business-place-activation');

exports.api = functions.https.onRequest(api());

exports.approveOrRejectFinances = functions.firestore
  .document('finances/{financeId}')
  .onCreate((change, context) => approveOrRejectFinances(change, context));

exports.notifyAdminAboutNewUser = functions.firestore
  .document('profiles/{userId}')
  .onCreate((change, context) => notifyAdminAboutNewUser(change, context));

exports.checkClientInAmo = functions.firestore
  .document('profiles/{userId}')
  .onCreate((change, context) => checkClientInAmo(change, context));

// exports.updateFollowerInfoInAllParents = functions.firestore
//   .document('profiles/{uid}')
//   .onUpdate((change, context) => updateFollowerInfoInAllParents(change, context));

// exports.handleBusinessPlaceActivation = functions.firestore
//   .document('business_places/{bpId}')
//   .onUpdate((change, context) => handleBusinessPlaceActivation(change, context));
